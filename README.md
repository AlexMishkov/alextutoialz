# AlexTutoialZ

This is a site for choosing tutorials for different technologies, and filtering them as you wish with the options in the filter. 
You can login with any Google account but for Github and Facebook i need terms and conditions and a personal domain to get approved 
from Facebook or Github.

You can also manualy create an account with a valid email.
After you log in you can add tutorials youself that need to be approved from the admin. When the admin approves it, it will send you an automated email that it is approved. And if it is not approved it will send you an email with explanation why it is not approved.

There is a special button that only the admin can see in the navbar that takes you to the admin panel.
This site is also hosted and the link is http://alextutorialz.000webhostapp.com/programming
For testing purposes there are only Tutorials for Python. 
