<?php

namespace App\Http\Controllers;

use App\Tutorial;
use App\Mail\RejectMail;
use App\Mail\ApproveMail;
use Illuminate\Http\Request;
use App\Http\Requests\RejectRequest;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function index(){
        $unapproved = Tutorial::with('subCategories', 'languages', 'levels', 'types', 'media')->where('approved', null)->get();
        $approved = Tutorial::with('subCategories', 'languages', 'levels', 'types', 'media')->where('approved', '!=', null)->get();
        return view('unapprovedTutorials', compact('unapproved', 'approved'));
    }

    public function approveTutorial(Tutorial $tutorial){
        $username = $tutorial->users->name;
        Mail::to($tutorial->users->email)->send(new ApproveMail($username));
        $tutorial->approved = true;
        $tutorial->save();
        return redirect()->back();
    }

    public function rejectTutorial(RejectRequest $request){
        $tutorial = Tutorial::where('id', $request->tutorialId)->first();
        $username = $tutorial->users->name;
        $message = $request->reject_message;
        $rejectData = ['username' => $username, 'message' => $message];
        Mail::to($tutorial->users->email)->send(new RejectMail($rejectData));
        $tutorial->delete();
        return redirect()->back();
    }

    public function deleteTutorial(Tutorial $tutorial){
        
        $tutorial->delete();
        return redirect()->back();
    }   
}
