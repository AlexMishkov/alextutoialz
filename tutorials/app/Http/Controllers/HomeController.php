<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
   
    public function showProgramming()
    {        
        $category = Category::where('name', 'Programming')->first();
        $data = SubCategory::with('categories')->where('category_id', $category->id )->get();

        return view('technologies', compact('data', 'category'));
    }

    public function showDataScience()
    {        
        $category = Category::where('name', 'Data Science')->first();
        $data = SubCategory::with('categories')->where('category_id', $category->id )->get();

        return view('technologies', compact('data', 'category'));
    }

    public function showDevops()
    {        
        $category = Category::where('name', 'DevOps')->first();
        $data = SubCategory::with('categories')->where('category_id', $category->id )->get();

        return view('technologies', compact('data', 'category'));
    }

    public function showDesign()
    {        
        $category = Category::where('name', 'Design')->first();
        $data = SubCategory::with('categories')->where('category_id', $category->id )->get();

        return view('technologies', compact('data', 'category'));
    }
}
