<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikesController extends Controller
{
    public function likes(Request $request){
        Like::updateOrCreate(
            ['tutorial_id' => $request->file, 'user_id' => Auth::user()->id,],
            ['liked' => true]
        );
        return redirect()->back();
    }

    public function unlike(Request $request){
        // dd($request->file);
        Like::where('tutorial_id', $request->file)
        ->where('user_id', Auth::user()->id)
        ->update([
            'liked' => false,
        ]);
    }

    public function getLikes(Request $request){
        // dd(Like::where('tutorial_id', 2)->where('liked', false)->get());
        return Like::where('tutorial_id', $request->file)->where('liked', true)->get();
    }
}
