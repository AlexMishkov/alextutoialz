<?php

namespace App\Http\Controllers;

use App\Like;
use App\Type;
use App\Level;
use App\Medium;
use App\Language;
use App\Tutorial;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TutorialRequest;

class TutorialController extends Controller
{
    public function showTutorials($slug){
        $subCategory = SubCategory::where('slug', $slug)->first();
        $randSubCategories = SubCategory::where('category_id', $subCategory->category_id)->where('id', '!=', $subCategory->id)->limit(6)->inRandomOrder()->get();
        $tutorials = Tutorial::with('subCategories', 'languages', 'levels', 'types', 'media', 'likes')->where('sub_category_id', $subCategory->id)->get();
        $free = count(DB::table('tutorial_type')->where('type_id', Type::where('type_name', 'Free')->first()->id)->get());
        $paid = count(DB::table('tutorial_type')->where('type_id', Type::where('type_name', 'Paid')->first()->id)->get());
        $video = count(DB::table('medium_tutorial')->where('medium_id', Medium::where('medium_name', 'Video')->first()->id)->get());
        $book = count(DB::table('medium_tutorial')->where('medium_id', Medium::where('medium_name', 'Book')->first()->id)->get());
        $beginner = count(Tutorial::where('level_id', Level::where('level_name', 'Beginner')->first()->id)->get());
        $advanced = count(Tutorial::where('level_id', Level::where('level_name', 'Advanced')->first()->id)->get());
        $english = count(Tutorial::where('language_id', Language::where('language_name', 'English')->first()->id)->get());
        $spanish = count(Tutorial::where('language_id', Language::where('language_name', 'Spanish')->first()->id)->get());
        $french = count(Tutorial::where('language_id', Language::where('language_name', 'French')->first()->id)->get());
        $german = count(Tutorial::where('language_id', Language::where('language_name', 'German')->first()->id)->get());
        return view('tutorials', compact('tutorials', 'subCategory', 'randSubCategories', 'free', 'paid', 'video', 'book', 'beginner', 'advanced', 'english', 'spanish', 'french', 'german'));
    }

    public function storeTutorial(TutorialRequest $request){
        // dd(Level::where('level_name', $request->input('level'))->first()->id);
        $types = Type::where('type_name', $request->input('free'))->orWhere('type_name', $request->input('paid'))->get();
        $media = Medium::where('medium_name', $request->input('video'))->orWhere('medium_name', $request->input('book'))->get();
        $tutorial = new Tutorial();
        $tutorial->name = $request->input('name');
        $tutorial->url = $request->input('url');
        $tutorial->user_id = Auth::user()->id;
        $tutorial->level_id = Level::where('level_name', $request->input('level'))->first()->id;
        $tutorial->language_id = Language::where('language_name', $request->input('language'))->first()->id;
        $tutorial->sub_category_id = SubCategory::where('name', $request->input('technology'))->first()->id;
        $tutorial->save();
        $tutorial->types()->attach($types);
        $tutorial->media()->attach($media);

        return redirect()->back();
    }
}
