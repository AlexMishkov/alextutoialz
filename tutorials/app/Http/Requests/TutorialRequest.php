<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TutorialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:128'],
            'url' => ['required', 'url'],
            'technology' => ['required', 'exists:sub_categories,name'],
            'free' => ['exists:types,type_name'],
            'paid' => ['exists:types,type_name'],
            'video' => ['exists:media,medium_name'],
            'book' => ['exists:media,medium_name'],
            'level' => ['exists:levels,level_name'],
            'language' => ['exists:languages,language_name'],
        ];
    }
}
