<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tutorial extends Model
{
    use SoftDeletes;
    protected $guarded = '';

    public function subCategories(){
        return $this->belongsTo(SubCategory::class, 'sub_category_id', 'id');
    }

    public function languages(){
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }

    public function users(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function levels(){
        return $this->belongsTo(Level::class, 'level_id', 'id');
    }

    public function types(){
        return $this->belongsToMany(Type::class);
    }

    public function media(){
        return $this->belongsToMany(Medium::class);
    }

    public function likes(){
        return $this->hasMany(Like::class, 'tutorial_id', 'id');
    }
    
}
