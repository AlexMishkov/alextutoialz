<?php

use App\Level;
use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levels = ['Beginner', 'Advanced'];

        foreach($levels as $level){
            Level::create([
                'level_name' => $level,
            ]);
        }
    }
}
