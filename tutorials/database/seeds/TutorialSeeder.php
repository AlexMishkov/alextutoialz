<?php

use App\Medium;
use App\Tutorial;
use App\Type;
use Illuminate\Database\Seeder;

class TutorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tutorials = [
            [
                'type' => Type::where('type_name', 'Free')->orWhere('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Video')->get(),
                'tutorialName' => '1 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 1,
                'tutorialLanguageId' => 2,
                'tutorialSubcategoryId' => 1,

            ],
            [
                'type' => Type::where('type_name', 'Free')->get(),
                'media' => Medium::where('medium_name', 'Video')->orWhere('medium_name', 'Book')->get(),
                'tutorialName' => '2 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 2,
                'tutorialLanguageId' => 1,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Book')->get(),
                'tutorialName' => '3 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 1,
                'tutorialLanguageId' => 1,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Free')->orWhere('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Video')->orWhere('medium_name', 'Book')->get(),
                'tutorialName' => '4 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 2,
                'tutorialLanguageId' => 2,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Free')->orWhere('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Video')->get(),
                'tutorialName' => '5 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 1,
                'tutorialLanguageId' => 2,
                'tutorialSubcategoryId' => 1,

            ],
            [
                'type' => Type::where('type_name', 'Free')->get(),
                'media' => Medium::where('medium_name', 'Video')->orWhere('medium_name', 'Book')->get(),
                'tutorialName' => '6 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 2,
                'tutorialLanguageId' => 1,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Book')->get(),
                'tutorialName' => '7 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 1,
                'tutorialLanguageId' => 1,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Free')->orWhere('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Video')->orWhere('medium_name', 'Book')->get(),
                'tutorialName' => '8 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 2,
                'tutorialLanguageId' => 2,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Free')->orWhere('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Video')->get(),
                'tutorialName' => '9 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 1,
                'tutorialLanguageId' => 2,
                'tutorialSubcategoryId' => 1,

            ],
            [
                'type' => Type::where('type_name', 'Free')->get(),
                'media' => Medium::where('medium_name', 'Video')->orWhere('medium_name', 'Book')->get(),
                'tutorialName' => '10 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 2,
                'tutorialLanguageId' => 1,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Book')->get(),
                'tutorialName' => '11 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 1,
                'tutorialLanguageId' => 1,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Free')->orWhere('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Video')->orWhere('medium_name', 'Book')->get(),
                'tutorialName' => '12 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 2,
                'tutorialLanguageId' => 2,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Free')->orWhere('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Video')->get(),
                'tutorialName' => '13 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 1,
                'tutorialLanguageId' => 2,
                'tutorialSubcategoryId' => 1,

            ],
            [
                'type' => Type::where('type_name', 'Free')->get(),
                'media' => Medium::where('medium_name', 'Video')->orWhere('medium_name', 'Book')->get(),
                'tutorialName' => '14 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 2,
                'tutorialLanguageId' => 1,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Book')->get(),
                'tutorialName' => '15 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 1,
                'tutorialLanguageId' => 1,
                'tutorialSubcategoryId' => 1,
            ],
            [
                'type' => Type::where('type_name', 'Free')->orWhere('type_name', 'Paid')->get(),
                'media' => Medium::where('medium_name', 'Video')->orWhere('medium_name', 'Book')->get(),
                'tutorialName' => '16 python tutorial',
                'tutorialUrl' => 'https://www.youtube.com/watch?v=woVJ4N5nl_s',
                'tutorialLevelId' => 2,
                'tutorialLanguageId' => 2,
                'tutorialSubcategoryId' => 1,
            ],
        ];

        foreach ($tutorials as $tutorial) {
            $types = $tutorial['type'];
            $media = $tutorial['media'];
            $newTutorial = new Tutorial();
            $newTutorial->name = $tutorial['tutorialName'];
            $newTutorial->url = $tutorial['tutorialUrl'];
            $newTutorial->user_id = 1;
            $newTutorial->level_id = $tutorial['tutorialLevelId'];
            $newTutorial->language_id = $tutorial['tutorialLanguageId'];
            $newTutorial->sub_category_id = $tutorial['tutorialSubcategoryId'];
            $newTutorial->save();
            $newTutorial->types()->attach($types);
            $newTutorial->media()->attach($media);
        }

    }
}
