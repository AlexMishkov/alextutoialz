<?php

use App\Type;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tpes = ['Free', 'Paid'];

        foreach($tpes as $type){
            Type::create([
                'type_name' => $type,
            ]);
        }
    }
}
