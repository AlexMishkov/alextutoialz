<?php

use App\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = ['English', 'Spanish', 'French', 'German'];

        foreach($languages as $language){
            Language::create([
                'language_name' => $language,
            ]);
        }
    }
}
