<?php

use App\SubCategory;
use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subCategories = [
            [
                'name' => 'Python',
                'image' => 'https://hackr.io/tutorials/python/logo-python.svg?ver=1562823957',
                'parent_id' => 1,
            ],
            [
                'name' => 'JavaScript',
                'image' => 'https://hackr.io/tutorials/javascript/logo-javascript.svg?ver=1610114223',
                'parent_id' => 1,
            ],
            [
                'name' => 'HTML 5',
                'image' => 'https://hackr.io/tutorials/html-5/logo-html-5.svg?ver=1587977020',
                'parent_id' => 1,
            ],
            [
                'name' => 'Java',
                'image' => 'https://hackr.io/tutorials/java/logo-java.svg?ver=1610114251',
                'parent_id' => 1,
            ],
            [
                'name' => 'CSS',
                'image' => 'https://hackr.io/tutorials/css/logo-css.svg?ver=1587721903',
                'parent_id' => 1,
            ],
            [
                'name' => 'C++',
                'image' => 'https://hackr.io/tutorials/c-plus-plus/logo-c-plus-plus.svg?ver=1579861999',
                'parent_id' => 1,
            ],
            [
                'name' => 'Data Structures And Algorithms',
                'image' => 'https://hackr.io/tutorials/data-structures-algorithms/logo-data-structures-algorithms.svg?ver=1587721467',
                'parent_id' => 1,
            ],
            [
                'name' => 'React',
                'image' => 'https://hackr.io/tutorials/react/logo-react.svg?ver=1610114789',
                'parent_id' => 1,
            ],
            [
                'name' => 'Node.js',
                'image' => 'https://hackr.io/tutorials/node-js/logo-node-js.svg?ver=1610118577',
                'parent_id' => 1,
            ],
            [
                'name' => 'C',
                'image' => 'https://hackr.io/tutorials/c/logo-c.svg?ver=1610115043',
                'parent_id' => 1,
            ],
            [
                'name' => 'PHP',
                'image' => 'https://hackr.io/tutorials/php/logo-php.svg?ver=1610114282',
                'parent_id' => 1,
            ],
            [
                'name' => 'Django',
                'image' => 'https://hackr.io/tutorials/django/logo-django.svg?ver=1610114943',
                'parent_id' => 1,
            ],
            [
                'name' => 'Intro to Programming',
                'image' => 'https://hackr.io/tutorials/intro-to-programming/logo-intro-to-programming.svg?ver=1587978368',
                'parent_id' => 1,
            ],
            [
                'name' => 'Android Development',
                'image' => 'https://hackr.io/tutorials/android-development/logo-android-development.svg?ver=1610114569',
                'parent_id' => 1,
            ],
            [
                'name' => 'MySQL',
                'image' => 'https://hackr.io/tutorials/mysql/logo-mysql.svg?ver=1587981026',
                'parent_id' => 1,
            ],
            [
                'name' => 'Bootstrap',
                'image' => 'https://hackr.io/tutorials/bootstrap/logo-bootstrap.svg?ver=1555328097',
                'parent_id' => 1,
            ],
            [
                'name' => 'SQL',
                'image' => 'https://hackr.io/tutorials/sql/logo-sql.svg?ver=1610118638',
                'parent_id' => 1,
            ],
            [
                'name' => 'C#',
                'image' => 'https://hackr.io/tutorials/c-sharp/logo-c-sharp.svg?ver=1587718651',
                'parent_id' => 1,
            ],
            [
                'name' => 'jQuery',
                'image' => 'https://hackr.io/tutorials/jquery/logo-jquery.svg?ver=1557130183',
                'parent_id' => 1,
            ],
            [
                'name' => 'TypeScript',
                'image' => 'https://hackr.io/tutorials/typescript/logo-typescript.svg?ver=1610119323',
                'parent_id' => 1,
            ],
            [
                'name' => 'React Native',
                'image' => 'https://hackr.io/tutorials/react-native/logo-react-native.svg?ver=1610114807',
                'parent_id' => 1,
            ],
            [
                'name' => 'Angular',
                'image' => 'https://hackr.io/tutorials/angular/logo-angular.svg?ver=1590863531',
                'parent_id' => 1,
            ],
            [
                'name' => 'Information Security & Ethical Hacking',
                'image' => 'https://hackr.io/tutorials/information-security-ethical-hacking/logo-information-security-ethical-hacking.svg?ver=1610120084',
                'parent_id' => 1,
            ],
            [
                'name' => 'Git',
                'image' => 'https://hackr.io/tutorials/git/logo-git.svg?ver=1587724445',
                'parent_id' => 1,
            ],
            [
                'name' => 'Unity',
                'image' => 'https://hackr.io/tutorials/unity/logo-unity.svg?ver=1610118495',
                'parent_id' => 1,
            ],
            [
                'name' => 'Flutter',
                'image' => 'https://hackr.io/tutorials/flutter/logo-flutter.svg?ver=1610118714',
                'parent_id' => 1,
            ],
            [
                'name' => 'PostgreSQL',
                'image' => 'https://hackr.io/tutorials/postgresql/logo-postgresql.svg?ver=1555390079',
                'parent_id' => 1,
            ],
            [
                'name' => 'Kotlin',
                'image' => 'https://hackr.io/tutorials/kotlin/logo-kotlin.svg?ver=1555389603',
                'parent_id' => 1,
            ],
            [
                'name' => 'Laravel',
                'image' => 'https://hackr.io/tutorials/laravel/logo-laravel.svg?ver=1603206644',
                'parent_id' => 1,
            ],
            [
                'name' => 'Redux',
                'image' => 'https://hackr.io/tutorials/redux/logo-redux.svg?ver=1551354757',
                'parent_id' => 1,
            ],
            [
                'name' => 'iOS Swift',
                'image' => 'https://hackr.io/tutorials/ios-swift/logo-ios-swift.svg?ver=1587978037',
                'parent_id' => 1,
            ],
            [
                'name' => 'Vue.js',
                'image' => 'https://hackr.io/tutorials/vue-js/logo-vue-js.svg?ver=1555390471',
                'parent_id' => 1,
            ],
            [
                'name' => 'Go',
                'image' => 'https://hackr.io/tutorials/golang/logo-golang.svg?ver=1555401655',
                'parent_id' => 1,
            ],
            [
                'name' => 'Visual Basic .NET',
                'image' => 'https://hackr.io/tutorials/visual-basic-net-vb/logo-visual-basic-net-vb.svg?ver=1551354757',
                'parent_id' => 1,
            ],
            [
                'name' => 'Unreal Engine',
                'image' => 'https://hackr.io/tutorials/unreal-engine/logo-unreal-engine.svg?ver=1610118993',
                'parent_id' => 1,
            ],
            [
                'name' => 'Ruby',
                'image' => 'https://hackr.io/tutorials/ruby/logo-ruby.svg?ver=1588062397',
                'parent_id' => 1,
            ],
            [
                'name' => 'Software Testing',
                'image' => 'https://hackr.io/tutorials/software-testing/logo-software-testing.svg?ver=1553834045',
                'parent_id' => 1,
            ],
            [
                'name' => 'Ruby on Rails',
                'image' => 'https://hackr.io/tutorials/ruby-on-rails/logo-ruby-on-rails.svg?ver=1588062348',
                'parent_id' => 1,
            ],
            [
                'name' => 'Scala',
                'image' => 'https://hackr.io/tutorials/scala/logo-scala.svg?ver=1610119371',
                'parent_id' => 1,
            ],
            [
                'name' => 'ASP.NET',
                'image' => 'https://hackr.io/tutorials/asp-net/logo-asp-net.svg?ver=1555328015',
                'parent_id' => 1,
            ],
            [
                'name' => 'Java Spring Framework',
                'image' => 'https://hackr.io/tutorials/java-spring-framework/logo-java-spring-framework.svg?ver=1555390385',
                'parent_id' => 1,
            ],
            [
                'name' => 'Ionic',
                'image' => 'https://hackr.io/tutorials/ionic/logo-ionic.svg?ver=1610119732',
                'parent_id' => 1,
            ],
            [
                'name' => 'Blockchain Programming',
                'image' => 'https://hackr.io/tutorials/blockchain/logo-blockchain.svg?ver=1587106554',
                'parent_id' => 1,
            ],
            [
                'name' => 'Elastic Search',
                'image' => 'https://hackr.io/tutorials/elastic-search/logo-elastic-search.svg?ver=1583389547',
                'parent_id' => 1,
            ],
            [
                'name' => 'Deno',
                'image' => 'https://hackr.io/tutorials/deno/logo-deno.svg?ver=1590289614',
                'parent_id' => 1,
            ],
            [
                'name' => 'Selenium',
                'image' => 'https://hackr.io/tutorials/selenium/logo-selenium.svg?ver=1556099522',
                'parent_id' => 1,
            ],
            [
                'name' => 'SEO',
                'image' => 'https://hackr.io/tutorials/seo/logo-seo.svg?ver=1550050459',
                'parent_id' => 1,
            ],
            [
                'name' => 'Bitcoin',
                'image' => 'https://hackr.io/tutorials/bitcoin/logo-bitcoin.svg?ver=1587717623',
                'parent_id' => 1,
            ],
            [
                'name' => 'Arduino',
                'image' => 'https://hackr.io/tutorials/arduino/logo-arduino.svg?ver=1551766080',
                'parent_id' => 1,
            ],
            [
                'name' => 'Google Analytics',
                'image' => 'https://hackr.io/tutorials/google-analytics/logo-google-analytics.svg?ver=1587726244',
                'parent_id' => 1,
            ],
            [
                'name' => 'Electron',
                'image' => 'https://hackr.io/tutorials/electron/logo-electron.svg?ver=1551354757',
                'parent_id' => 1,
            ],
            [
                'name' => 'Website Performance',
                'image' => 'https://hackr.io/tutorials/website-performance/logo-website-performance.svg?ver=1592914803',
                'parent_id' => 1,
            ],
            [
                'name' => 'Alexa Skills Kit',
                'image' => 'https://hackr.io/tutorials/alexa-skills-kit-ask/logo-alexa-skills-kit-ask.svg?ver=1554107807',
                'parent_id' => 1,
            ],
            [
                'name' => 'Elixir',
                'image' => 'https://hackr.io/tutorials/elixir/logo-elixir.svg?ver=1579691321',
                'parent_id' => 1,
            ],
            [
                'name' => 'Virtual Reality',
                'image' => 'https://hackr.io/tutorials/virtual-reality/logo-virtual-reality.svg?ver=1563364635',
                'parent_id' => 1,
            ],
            [
                'name' => 'VHDL',
                'image' => 'https://hackr.io/tutorials/vhdl/logo-vhdl.svg?ver=1590509528',
                'parent_id' => 1,
            ],
            [
                'name' => 'Growth Hacking',
                'image' => 'https://hackr.io/tutorials/growth-hacking/logo-growth-hacking.svg?ver=1587976317',
                'parent_id' => 1,
            ],
            [
                'name' => 'Solidity',
                'image' => 'https://hackr.io/tutorials/solidity/logo-solidity.svg?ver=1553831680',
                'parent_id' => 1,
            ],
            [
                'name' => 'Verilog',
                'image' => 'https://hackr.io/tutorials/verilog/logo-verilog.svg?ver=1590508305',
                'parent_id' => 1,
            ],
            [
                'name' => 'Xamarin',
                'image' => 'https://hackr.io/tutorials/xamarin/logo-xamarin.svg?ver=1588065225',
                'parent_id' => 1,
            ],
            [
                'name' => 'Machine Learning',
                'image' => 'https://hackr.io/tutorials/machine-learning-ml/logo-machine-learning-ml.svg?ver=1559644608',
                'parent_id' => 2,
            ],
            [
                'name' => 'Data Science',
                'image' => 'https://hackr.io/tutorials/data-science/logo-data-science.svg?ver=1555775500',
                'parent_id' => 2,
            ],
            [
                'name' => 'Deep Learning',
                'image' => 'https://hackr.io/tutorials/deep-learning/logo-deep-learning.svg?ver=1576625961',
                'parent_id' => 2,
            ],
            [
                'name' => 'TensorFlow',
                'image' => 'https://hackr.io/tutorials/tensorflow/logo-tensorflow.svg?ver=1563364741',
                'parent_id' => 2,
            ],
            [
                'name' => 'R',
                'image' => 'https://hackr.io/tutorials/r/logo-r.svg?ver=1563444139',
                'parent_id' => 2,
            ],
            [
                'name' => 'Hadoop',
                'image' => 'https://hackr.io/tutorials/hadoop-big-data/logo-hadoop-big-data.svg?ver=1555234281',
                'parent_id' => 2,
            ],
            [
                'name' => 'Apache Spark',
                'image' => 'https://hackr.io/tutorials/apache-spark/logo-apache-spark.svg?ver=1555327926',
                'parent_id' => 2,
            ],
            [
                'name' => 'Linux System Administration',
                'image' => 'https://hackr.io/tutorials/linux-system-administration/logo-linux-system-administration.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Docker',
                'image' => 'https://hackr.io/tutorials/docker/logo-docker.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'AWS',
                'image' => 'https://hackr.io/tutorials/amazon-web-services-aws/logo-amazon-web-services-aws.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'DevOps',
                'image' => 'https://hackr.io/tutorials/devops/logo-devops.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Windows Server Administration',
                'image' => 'https://hackr.io/tutorials/windows-server-administration/logo-windows-server-administration.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Google Cloud Platform',
                'image' => 'https://hackr.io/tutorials/google-cloud-platform/logo-google-cloud-platform.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Kubernetes',
                'image' => 'https://hackr.io/tutorials/kubernetes/logo-kubernetes.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'System Architecture',
                'image' => 'https://hackr.io/tutorials/system-architecture/logo-system-architecture.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Microsoft Azure',
                'image' => 'https://hackr.io/tutorials/microsoft-azure/logo-microsoft-azure.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Jenkins',
                'image' => 'https://hackr.io/tutorials/jenkins/logo-jenkins.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Serverless Computing',
                'image' => 'https://hackr.io/tutorials/serverless-computing/logo-serverless-computing.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Ansible',
                'image' => 'https://hackr.io/tutorials/ansible/logo-ansible.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Apache Kafka',
                'image' => 'https://hackr.io/tutorials/apache-kafka/logo-apache-kafka.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Vagrant',
                'image' => 'https://hackr.io/tutorials/vagrant/logo-vagrant.svg',
                'parent_id' => 3,
              ],
              [
                'name' => 'Puppet',
                'image' => 'https://hackr.io/tutorials/puppet/logo-puppet.svg',
                'parent_id' => 3,
              ],
              [
                "name" => "Game Design",
                "image" => "https://hackr.io/tutorials/game-design/logo-game-design.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Photoshop",
                "image" => "https://hackr.io/tutorials/photoshop/logo-photoshop.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Illustrator",
                "image" => "https://hackr.io/tutorials/adobe-illustrator/logo-adobe-illustrator.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Blender",
                "image" => "https://hackr.io/tutorials/blender/logo-blender.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Adobe Premier Pro",
                "image" => "https://hackr.io/tutorials/adobe-premier-pro/logo-adobe-premier-pro.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Adobe Experience Design",
                "image" => "https://hackr.io/tutorials/adobe-experience-design/logo-adobe-experience-design.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Adobe After Effects",
                "image" => "https://hackr.io/tutorials/adobe-after-effects/logo-adobe-after-effects.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Adobe Lightroom",
                "image" => "https://hackr.io/tutorials/adobe-lightroom/logo-adobe-lightroom.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Adobe Indesign",
                "image" => "https://hackr.io/tutorials/adobe-indesign/logo-adobe-indesign.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "User Experience Design",
                "image" => "https://hackr.io/tutorials/user-experience-design/logo-user-experience-design.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Cinema 4D",
                "image" => "https://hackr.io/tutorials/cinema-4d/logo-cinema-4d.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "User Interface Design",
                "image" => "https://hackr.io/tutorials/user-interface-design/logo-user-interface-design.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Figma",
                "image" => "https://hackr.io/tutorials/figma/logo-figma.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Sketch",
                "image" => "https://hackr.io/tutorials/sketch/logo-sketch.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Prototyping",
                "image" => "https://hackr.io/tutorials/prototyping/logo-prototyping.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Design Thinking",
                "image" => "https://hackr.io/tutorials/design-thinking/logo-design-thinking.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "UX Research",
                "image" => "https://hackr.io/tutorials/ux-research/logo-ux-research.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Sketchbook Pro",
                "image" => "https://hackr.io/tutorials/sketchbook-pro/logo-sketchbook-pro.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Wireframing",
                "image" => "https://hackr.io/tutorials/wireframing/logo-wireframing.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Autodesk Maya",
                "image" => "https://hackr.io/tutorials/autodesk-maya/logo-autodesk-maya.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Product Design",
                "image" => "https://hackr.io/tutorials/product-design/logo-product-design.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Inkscape",
                "image" => "https://hackr.io/tutorials/inkscape/logo-inkscape.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Interaction Design",
                "image" => "https://hackr.io/tutorials/interaction-design/logo-interaction-design.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Autodesk 3ds Max",
                "image" => "https://hackr.io/tutorials/autodesk-3ds-max/logo-autodesk-3ds-max.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Color Theory",
                "image" => "https://hackr.io/tutorials/color-theory/logo-color-theory.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Industrial Design",
                "image" => "https://hackr.io/tutorials/industrial-design/logo-industrial-design.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Daz Studio",
                "image" => "https://hackr.io/tutorials/daz-studio/logo-daz-studio.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Typography",
                "image" => "https://hackr.io/tutorials/typography/logo-typography.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Information Architecture",
                "image" => "https://hackr.io/tutorials/information-architecture/logo-information-architecture.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "InVision",
                "image" => "https://hackr.io/tutorials/invision/logo-invision.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Drawing",
                "image" => "https://hackr.io/tutorials/drawing/logo-drawing.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Affinity Photo",
                "image" => "https://hackr.io/tutorials/affinity-photo/logo-affinity-photo.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Content Strategy",
                "image" => "https://hackr.io/tutorials/content-strategy/logo-content-strategy.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "UX Pin",
                "image" => "https://hackr.io/tutorials/ux-pin/logo-ux-pin.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Digital Painting",
                "image" => "https://hackr.io/tutorials/digital-painting/logo-digital-painting.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Affinity Designer",
                "image" => "https://hackr.io/tutorials/affinity-designer/logo-affinity-designer.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Branding",
                "image" => "https://hackr.io/tutorials/branding/logo-branding.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Framer",
                "image" => "https://hackr.io/tutorials/framer/logo-framer.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Origami Studio",
                "image" => "https://hackr.io/tutorials/origami-studio/logo-origami-studio.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Marvel",
                "image" => "https://hackr.io/tutorials/marvel/logo-marvel.svg",
                'parent_id' => 4,
              ],
              [
                "name" => "Principle",
                "image" => "https://hackr.io/tutorials/principle/logo-principle.svg",
                'parent_id' => 4,
              ]
        ];

        foreach($subCategories as $subCategory){
            SubCategory::create([
                'name' => $subCategory['name'],
                'slug' => Str::slug($subCategory['name']),
                'image' => $subCategory['image'],
                'category_id' => $subCategory['parent_id'],
            ]);
        }
    }
}
