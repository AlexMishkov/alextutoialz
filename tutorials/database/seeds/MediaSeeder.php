<?php

use App\Medium;
use Illuminate\Database\Seeder;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $media = ['Video', 'Book',];

        foreach($media as $medium){
            Medium::create([
                'medium_name' => $medium,
            ]);
        }
    }
}
