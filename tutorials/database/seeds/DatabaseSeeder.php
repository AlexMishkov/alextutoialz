<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(SubCategorySeeder::class);
        $this->call(LanguageSeeder::class);
        $this->call(LevelSeeder::class);
        $this->call(MediaSeeder::class);
        $this->call(TypeSeeder::class);
        $this->call(TutorialSeeder::class);

    }
}
