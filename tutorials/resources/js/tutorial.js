$(document).ready(function () {

    filterCheckbox('free');
    filterCheckbox('paid');
    filterCheckbox('video');
    filterCheckbox('book');
    levelFilterCheckbox('beginner');
    levelFilterCheckbox('advanced');
    levelFilterCheckbox('english');
    levelFilterCheckbox('spanish');
    levelFilterCheckbox('french');
    levelFilterCheckbox('german');

    $('span.Free').parent().parent().parent().parent().parent().parent().addClass('Free');
    $('span.Video').parent().parent().parent().parent().parent().parent().addClass('Video');
    $('span.Paid').parent().parent().parent().parent().parent().parent().addClass('Paid');
    $('span.Book').parent().parent().parent().parent().parent().parent().addClass('Book');
    $('span.Beginner').parent().parent().parent().parent().parent().parent().addClass('Beginner');
    $('span.Advanced').parent().parent().parent().parent().parent().parent().addClass('Advanced');
    $('span.English').parent().parent().parent().parent().parent().parent().addClass('English');
    $('span.Spanish').parent().parent().parent().parent().parent().parent().addClass('Spanish');
    $('span.French').parent().parent().parent().parent().parent().parent().addClass('French');
    $('span.German').parent().parent().parent().parent().parent().parent().addClass('German');
    $('.tutorials').addClass('shown-tutorials');
    paginate();

    filterNumbers();


    





});