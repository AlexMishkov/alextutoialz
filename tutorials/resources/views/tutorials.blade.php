@php
use App\Like;
use Illuminate\Support\Facades\Auth;

@endphp
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-2">
            {{-- *******Technology Hero --}}
            <div class="col-md-12 d-flex py-5 technology-intro rounded">
                <img src="{{ $subCategory->image }}" style="max-width: 70px">
                <div class="ml-4">
                    <h2>{{ $subCategory->name }} Tutorials and Courses</h2>
                    <p class="text-muted">Learn {{ $subCategory->name }} online from the best {{ $subCategory->name }}
                        tutorials submited & voted by the comunity</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                {{-- This is the filter bar --}}
                <div class="border-top  d-none border-bottom mt-4 filter-bar">
                    <div class="d-flex py-2 mt-1">
                        <h5 class="font-weight-bold">Filter: </h5>
                    </div>
                    <div class="filter-bar-content  ml-4 pt-2" id="filter-bar-content">

                    </div>
                </div>
            </div>
            {{-- This is the filter --}}
            <div class="row mt-4">
                <div class="col-md-2 pl-0 ">
                    <div class="filter-border rounded px-2">
                        <div class="filter-underline mt-2">
                            <h6 class="font-weight-bold">Filter Courses</h6>
                        </div>
                        <div class="mt-2">
                            <h6 class="font-weight-bold">Type of course</h6>
                            <div class="d-flex mb-3 ">
                                <input type="checkbox" class="checkboxes" id="free">
                                <label class="ml-2 free-label"><label>
                            </div>
                            <div class="d-flex mb-3">
                                <input type="checkbox" class="checkboxes" id="paid">
                                <label class="ml-2 paid-label"></label>
                            </div>
                            <h6 class="font-weight-bold">Medium</h6>
                            <div class="d-flex mb-3">
                                <input type="checkbox" class="checkboxes" id="video">
                                <label class="ml-2 video-label"></label>
                            </div>
                            <div class="d-flex mb-3">
                                <input type="checkbox" class="checkboxes" id="book">
                                <label class="ml-2 book-label"></label>
                            </div>
                            <h6 class="font-weight-bold">Level</h6>
                            <div class="d-flex mb-3">
                                <input type="checkbox" class="checkboxes" id="beginner">
                                <label class="ml-2 beginner-label"></label>
                            </div>
                            <div class="d-flex mb-3">
                                <input type="checkbox" class="checkboxes" id="advanced">
                                <label class="ml-2 advanced-label">Advanced ({{ !$advanced ? 0 : $advanced }})</label>
                            </div>
                            <h6 class="font-weight-bold">Language</h6>
                            <div class="d-flex mb-3">
                                <input type="checkbox" class="checkboxes" id="english">
                                <label class="ml-2 english-label">English ({{ !$english ? 0 : $english }})</label>
                            </div>
                            <div class="d-flex mb-3">
                                <input type="checkbox" class="checkboxes" id="spanish">
                                <label class="ml-2 spanish-label">Spanish ({{ !$spanish ? 0 : $spanish }})</label>
                            </div>
                            <div class="d-flex mb-3">
                                <input type="checkbox" class="checkboxes" id="french">
                                <label class="ml-2 french-label">French ({{ !$french ? 0 : $french }})</label>
                            </div>
                            <div class="d-flex mb-3">
                                <input type="checkbox" class="checkboxes" id="german">
                                <label class="ml-2 german-label">German ({{ !$german ? 0 : $german }})</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 px-0 border rounded">
                    <div class="border d-flex justify-content-between rounded-top">
                        <div>
                            <p class="pl-3 my-2">Top {{ $subCategory->name }} Tutorials</p>
                        </div>
                        <div>
                            <button class="btn btn-no-outline btn-transparent">Upvotes <i
                                    class="fa fa-arrow-down"></i></button>
                            <span>|</span>
                            <button class="btn btn-no-outline btn-transparent">Recent <i
                                    class="fa fa-arrow-down"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        {{-- *******Listing all tutorials --}}
                        @foreach ($tutorials as $tutorial)
                            @if ($tutorial->approved)
                                <div class="col-lg-4 col-12 tutorials ">
                                    <div class="col-md-12 mt-4 rounded tutorial-card">
                                        <div class="row">
                                            <div class="col-12" style="height: 25vh">
                                                <div class="row rounded border-bottom border-right pr-4 mx-2 pb-3 pt-3">
                                                    <div class="col-9">
                                                        <h4>{{ $tutorial->name }}</h4>
                                                    </div>
                                                    <div class="col-md-2 offset-md-1">
                                                        <a href="{{ $tutorial->url }}" target="_blank"
                                                            class="btn btn-sm btn-primary">View</a>
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    {{-- *******Listing tags of tutorials --}}
                                                    <div class="col-md-12">
                                                        <span
                                                            class="border rounded text-primary {{ $tutorial->levels->level_name }}">{{ $tutorial->levels->level_name }}</span>
                                                        <span
                                                            class="border rounded text-primary {{ $tutorial->languages->language_name }}">{{ $tutorial->languages->language_name }}</span>
                                                        @foreach ($tutorial->types as $type)
                                                            <span
                                                                class="border rounded text-primary {{ $type->type_name }}">{{ $type->type_name }}</span>
                                                        @endforeach
                                                        @foreach ($tutorial->media as $medium)
                                                            <span
                                                                class="border rounded text-primary {{ $medium->medium_name }}">{{ $medium->medium_name }}</span>
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">
                                                        <p>By: {{ $tutorial->users->name }}</p>
                                                        <p>Created at: {{ $tutorial->created_at }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            {{-- *******The like system --}}
                                                    @php
                                                    if(Auth::user()){
                                                        $checkLike = Like::where('user_id', Auth::user()->id)
                                                            ->where('tutorial_id', $tutorial->id)
                                                            ->where('liked', true)
                                                            ->first();
                                                    }
                                                        $likeNum = Like::where('tutorial_id', $tutorial->id)->where('liked', true)->get()
                                                    @endphp
                                            <div class="col-12 card-footer py-1  d-flex justify-content-between">
                                                @if (!Auth::user())
                                                    <button class="btn btn-sm like-btn rounded border text-decoration-none"
                                                        data-toggle="modal" data-target="#login-register">
                                                        <span class='like-span'>{{ count($likeNum) }}</span>
                                                        Likes <i class="fa fa-thumbs-up"></i></button>
                                                @elseif(Auth::user())
                                                    @if ($checkLike)

                                                        <form action='{{ route('unlike') }}' method='POST'
                                                            class='unlike-form'>
                                                            @csrf
                                                            
                                                            <input class='d-none tutorialId' type='number'
                                                                name='tutorial_id' value='{{ $tutorial->id }}'></input>
                                                            <span
                                                                class='like-span'>{{ count($likeNum) }}</span>
                                                            <button
                                                                class='btn btn-success btn-sm unlike-btn rounded border text-decoration-none'>

                                                                <span>Like <i class="fa fa-thumbs-up"></i></span> </button>
                                                            
                                                        </form>

                                                        <form action="{{ route('likes') }}" method="POST"
                                                            class="like-form" style="display: none">
                                                            @csrf
                                                            
                                                            <input class='d-none tutorialId' type='number'
                                                                name='tutorial_id' value='{{ $tutorial->id }}'></input>
                                                            <span
                                                                class='like-span'>{{ count($likeNum) }}</span>
                                                            <button
                                                                class="btn btn-sm like-btn rounded border text-decoration-none">

                                                                <span>Like <i class='fa fa-thumbs-up'></i></span></button>
                                                            
                                                        </form>

                                                    @else

                                                        <form action="{{ route('likes') }}" method="POST"
                                                            class="like-form">
                                                            @csrf
                                                            
                                                            <input class='d-none tutorialId' type='number'
                                                                name='tutorial_id' value='{{ $tutorial->id }}'></input>
                                                                <span
                                                                class='like-span'>{{ count($likeNum) }}</span>
                                                            <button
                                                                class="btn btn-sm like-btn rounded border text-decoration-none">

                                                                <span>Like <i class="fa fa-thumbs-up"></i></span></button>
                                                            
                                                        </form>

                                                        <form action='{{ route('unlike') }}' method='POST'
                                                            class='unlike-form' style="display: none">
                                                            @csrf
                                                            
                                                            <input class='d-none tutorialId' type='number'
                                                                name='tutorial_id' value='{{ $tutorial->id }}'></input>
                                                            <span
                                                                class='like-span'>{{ count($likeNum) }}</span>
                                                            <button
                                                                class='btn btn-success btn-sm unlike-btn rounded border text-decoration-none'>
                                                                <span>Like <i class="fa fa-thumbs-up"></i></span></button>
                                                            
                                                        </form>
                                                    @endif
                                                @endif
                                                @if ($tutorial->approved)
                                                    <span><i class="fa fa-check-circle text-success"></i></span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                {{-- *******Pagination --}}
                <div class="pagination-container offset-2">
                    <nav>
                        <ul class="pagination">

                        </ul>
                    </nav>
                </div>
                <div class="col-md-9 px-0 d-flex justify-content-end offset-md-3">

                </div>
                <div class="col-md-9 px-0 offset-md-3">
                    <div class="row justify-content-between mt-4">
                        {{-- *******The 6 random technologies under this class --}}
                        <div class="col-12">
                            <h4 class="font-weight-bold">You might also be interested in </h4>
                        </div>
                        @foreach ($randSubCategories as $technology)
                            <div class="col-md-3 border d-flex align-items-center rounded technology ml-2 mt-4 py-2">
                                <a href="{{ route('show.tutorials', $technology->slug) }}"
                                    class="d-flex text-dark text-decoration-none">
                                    <img src="{{ $technology->image }}" class="align-self-center technology-image">
                                    <p class="align-self-center mt-3 ml-3">{{ $technology->name }}</p>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endsection

    {{-- *******Scripts --}}
    @section('scripts')
        <script src="{{ asset('js/functions.js') }}"></script>
        <script src="{{ asset('js/tutorial.js') }}"></script>
        
    @endsection
