@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row footer-adjustment">
            <div class="col-12">
                <div class="row">
                    {{-- *******Listing unapproved tutorials --}}
                    @foreach ($unapproved as $tutorial)
                        <div class="col-4">
                            <div class="col-md-12 mt-4 rounded border ">
                                <div class="row rounded border-bottom border-right pr-4 mx-2 pb-3 pt-3">
                                    <div class="col-9">
                                        <h4>{{ $tutorial->name }}</h4>
                                    </div>
                                    <div class="col-md-2 offset-md-1">
                                        <a href="{{ $tutorial->url }}" target="_blank"
                                            class="btn btn-sm btn-primary">View</a>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-12">
                                        <span
                                            class="border rounded text-primary {{ $tutorial->levels->level_name }}">{{ $tutorial->levels->level_name }}</span>
                                        <span
                                            class="border rounded text-primary {{ $tutorial->languages->language_name }}">{{ $tutorial->languages->language_name }}</span>
                                        @foreach ($tutorial->types as $type)
                                            <span
                                                class="border rounded text-primary {{ $type->type_name }}">{{ $type->type_name }}</span>
                                        @endforeach
                                        @foreach ($tutorial->media as $medium)
                                            <span
                                                class="border rounded text-primary {{ $medium->medium_name }}">{{ $medium->medium_name }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p>By: {{ $tutorial->users->name }}</p>
                                        <p>Created at: {{ $tutorial->created_at }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 card-footer d-flex justify-content-between">
                                        <span class="rounded border">{{ !$tutorial->upvotes ? 0 : $tutorial->upvotes }}
                                            Likes <i class="fa fa-thumbs-up"></i></span>
                                        @if ($tutorial->approved)
                                            <span><i class="fa fa-check-circle"></i></span>
                                        @endif
                                    </div>
                                    <div class="col-12 card-footer d-flex justify-content-between">
                                        {{-- *******Approving form --}}
                                        <form action="{{ route('approve.tutorial', $tutorial->id) }}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <button class="btn btn-sm btn-success">Approve</button>
                                        </form>
                                        {{-- *******Rejecting button(prompts a modal with form to write rejection reason and sends email automaticly) --}}
                                        <button class="btn btn-sm btn-danger btn-reject" data-toggle="modal"
                                            data-target="#rejectModal" tutorial_id="{{ $tutorial->id }}">Reject</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                        {{-- *******Listing all other tutorials --}}
                    @foreach ($approved as $tutorial)
                        <div class="col-4">
                            <div class="col-md-12 mt-4 rounded border ">
                                <div class="row rounded border-bottom border-right pr-4 mx-2 pb-3 pt-3">
                                    <div class="col-9">
                                        <h4>{{ $tutorial->name }}</h4>
                                    </div>
                                    <div class="col-md-2 offset-md-1">
                                        <a href="{{ $tutorial->url }}" target="_blank"
                                            class="btn btn-sm btn-primary">View</a>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-12">
                                        <span
                                            class="border rounded text-primary {{ $tutorial->levels->level_name }}">{{ $tutorial->levels->level_name }}</span>
                                        <span
                                            class="border rounded text-primary {{ $tutorial->languages->language_name }}">{{ $tutorial->languages->language_name }}</span>
                                        @foreach ($tutorial->types as $type)
                                            <span
                                                class="border rounded text-primary {{ $type->type_name }}">{{ $type->type_name }}</span>
                                        @endforeach
                                        @foreach ($tutorial->media as $medium)
                                            <span
                                                class="border rounded text-primary {{ $medium->medium_name }}">{{ $medium->medium_name }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p>By: {{ $tutorial->users->name }}</p>
                                        <p>Created at: {{ $tutorial->created_at }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 card-footer d-flex justify-content-between">
                                        <span class="rounded border">{{ !$tutorial->upvotes ? 0 : $tutorial->upvotes }}
                                            Likes <i class="fa fa-thumbs-up"></i></span>
                                        @if ($tutorial->approved)
                                            <span><i class="fa fa-check-circle"></i></span>
                                        @endif
                                    </div>
                                    <div class="col-12 card-footer d-flex justify-content-between">
                                        <form action="{{ route('delete.tutorial', $tutorial->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    {{-- *******Modal for reject message --}}
    <div class="modal fade" id="rejectModal" tabindex="-1" aria-labelledby="reject-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="reject-label">Reject form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('reject.tutorial') }}" method="POST">
                        @csrf
                        @method("DELETE")
                        <input type="number" class="d-none" id="tutorialId" name="tutorialId">
                        <div class="form-group">
                            <label for="reject_message">Enter reject message:</label>
                            <textarea class="form-control" name="reject_message" id="reject_message" rows="3"></textarea>
                          </div>
                          <button class="btn btn-block btn-danger">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/functions.js') }}"></script>
    <script src="{{ asset('js/admin.js') }}"></script>
@endsection
