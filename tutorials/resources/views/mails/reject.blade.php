<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tutorial Approved</title>
</head>
<body>
    <h4>Dear {{ $rejectedData['username'] }},</h4>
    <h4>Thank you for contacting TutorialZ</h4>
    <h5>Your tutorial has been rejected for the following reasons:</h5>
    <p>{{ $rejectedData['message'] }}</p>
</body>
</html>