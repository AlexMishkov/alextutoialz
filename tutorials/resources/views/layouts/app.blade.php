<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Fontawesome -->
    <script src="https://use.fontawesome.com/563bb0680c.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        {{-- *******Navbar start --}}
        <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
            <a class="navbar-brand ml-4" href="{{ url('/') }}"><img
                    src="{{ asset('Logos/Brainster-symbol32x32-02.png') }}" class="ml-4" alt=""> <br> <img
                    src="{{ asset('Logos/Brainster-symbol310x150-02.png') }}" style="width: 85px" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link nav-link-text {{ \Request::is('programming') ? 'active-nav' : '' }}"
                            href="{{ route('programming') }}"><i class="fa fa-code"></i>Programming<span
                                class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-text {{ \Request::is('data-science') ? 'active-nav' : '' }}"
                            href="{{ route('data-science') }}"><i class="fa fa-database"></i> Data Science<span
                                </span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-text {{ \Request::is('devops') ? 'active-nav' : '' }}"
                            href="{{ route('devops') }}"><i class="fa fa-angle-double-up"></i> DevOps<span
                                </span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-text {{ \Request::is('design') ? 'active-nav' : '' }}"
                            href="{{ route('design') }}"><i class="fa fa-paint-brush"></i> Design<span </span></a>
                    </li>
                </ul>
                {{-- *******Global search bar --}}
                <div class="input-group mb-2 search-bar">
                    <div class="input-group-prepend">
                        <div class="input-group-text border-right-0 bg-white">
                            <i class="fa fa-search text-primary"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control dropdown-toggle" id="globalSearch" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <div class="dropdown-menu w-100" style="display: none" id="globalSearchDropdown"
                        aria-labelledby="dropdownMenuButton">
                        <ul id="globalSearchList" style="list-style: none">

                        </ul>
                    </div>
                </div>
                <ul class="navbar-nav ml-auto">

                    {{-- *******Admin panel button --}}
                    @if (Auth::user() && Auth::user()->role == 'admin')
                        <a class="btn custom-nav-btn" href="{{ route('unapproved.tutorials') }}">View unapproved
                            tutorials</a>
                    @endif
                    @if (Auth::user())
                    <button class="btn custom-nav-btn" data-toggle="modal" data-target="#addTutorial"><i
                            class="fa fa-plus mr-1"></i>Submit a tutorial</button>
                        
                    @else
                    <button class="btn custom-nav-btn" data-toggle="modal" data-target="#login-register"><i
                        class="fa fa-plus mr-1"></i>Submit a tutorial</button>
                    @endif
                    <!-- Authentication Links -->
                    @guest
                        <button class="btn custom-nav-btn" data-toggle="modal" data-target="#login-register"></i>Sign
                            up/Sign</button>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link nav-link-text dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>


        <main class="py-4">
            @yield('content')
        </main>
        {{-- *******Footer --}}
        <footer class="py-4 text-center border-top mt-5">
            <h4>Made with &#10084;&#65039; for Brainster</h4>
            <small>&copy; Brainster 2021</small>
        </footer>
    </div>

    {{-- *******Modals --}}

    {{-- *******Modal for login and register --}}
    <div class="modal fade" id="login-register" tabindex="-1" aria-labelledby="login-register" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <h3 class="modal-title ml-auto font-weight-bold" id="login-register">Welcome to TutorialZ</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="text-center border-bottom">
                    <p class="text-muted ml-auto my-2">Signup to submit and upvote tutorials, follow topics, and more.
                    </p>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-10 offset-md-1 mt-2">
                            <a class="w-100 text-decoration-none social-button"
                                href="{{ route('github.login') }}"><img src="{{ asset('img/github-logo.png') }}"
                                    class="login-logos " alt=""><span
                                    class="text-decoration-none social-login-border py-2 pl-3 text-muted">Login with
                                    your Github account</span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 offset-md-1 mt-2">
                            <a class="w-100 text-decoration-none social-button" disabled
                                href="{{ route('facebook.login') }}"><img
                                    src="{{ asset('img/facebook-logo.png') }}" class="login-logos " alt=""><span
                                    class="text-decoration-none social-login-border py-2 pl-3 text-muted">Login with
                                    your Facebook account</span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 offset-md-1 mt-2">
                            <a class="w-100 text-decoration-none social-button"
                                href="{{ route('google.login') }}"><img src="{{ asset('img/google-logo.png') }}"
                                    class="login-logos " alt=""><span
                                    class="text-decoration-none social-login-border py-2 pl-3 text-muted">Login with
                                    your Google account</span></a>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('login') }}" id="login-form" style="display: none">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="email-login" type="email" placeholder="Enter email"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="password-login" type="password" placeholder="Enter password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <span>Don't have an account?</span>
                                <button type="button" class="btn text-primary" id="register-instead">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>

                    <form method="POST" action="{{ route('register') }}" id="register-form">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="name" placeholder="Enter full name" type="text"
                                    class="form-control @error('name') is-invalid @enderror" name="name"
                                    value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">


                            <div class="col-md-12">
                                <input -login placeholder="Enter your email" type="email"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="password" placeholder="Enter your password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="password-confirm" placeholder="Confirm your password" type="password"
                                    class="form-control" name="password_confirmation" required
                                    autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <span>Already have an account?</span>
                                <button type="button" class="btn text-primary" id="login-instead">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    {{-- *******Modal for submiting tutorial --}}
    <div class="modal fade" id="addTutorial" tabindex="-1" aria-labelledby="add-tutorial" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title ml-auto" id="add-tutorial">Submit your tutorial</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div class="text-center border-bottom">
                    <p class="text-muted ml-auto my-2">Submit tutorials at a click of a button.
                    </p>
                </div>
                <div class="modal-body">
                    <form action="{{ route('store.tutorial') }}" method="POST">
                        @csrf
                        <div class="input-group mb-2">
                            <input type="text" class="form-control" name="name" placeholder="Enter title (Required)"
                                id="">
                            @error('name')
                                <p class="text-danger">*{{ $errors->first('name') }}</p><br>
                            @enderror
                        </div>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text border-right-0 bg-white">
                                    <i class="fa fa-link" style="font-size: 20px"></i>
                                </div>
                            </div>
                            <input type="text" name="url" class="form-control dropdown-toggle"
                                placeholder="Place tutorial url here (Required)" id="tutorial-url">
                            @error('url')
                                <p class="text-danger">*{{ $errors->first('url') }}</p><br>
                            @enderror
                        </div>
                        <div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text border-right-0 bg-white">
                                        <i class="fa fa-search text-primary"></i>
                                    </div>
                                </div>
                                <input type="text" name="technology" placeholder="Which technology is it for (Required)"
                                    class="form-control dropdown-toggle" id="globalSearchTutorials"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="dropdown-menu w-100" style="display: none"
                                    id="globalSearchTutorialsDropdown" aria-labelledby="dropdownMenuButton">
                                    <ul id="globalSearchTutorialsList" style="list-style: none">

                                    </ul>
                                </div>
                                @error('technology')
                                    <p class="text-danger">*{{ $errors->first('technology') }}</p><br>
                                @enderror
                            </div>
                            <div class="mt-2">
                                <p>Tell us more about this tutorial/course.(Optional)</p>
                            </div>
                            <div class=" mt-2">
                                <div class="row mt-2 border-top border-bottom d-flex">
                                    <div class="col-md-4 d-flex">
                                        <p class="my-2">Tags:</p>
                                    </div>
                                    <div class="col-md-7 d-flex align-items-center">
                                        <input type="checkbox" name="free" value="Free" class="ml-2">Free
                                        <input type="checkbox" name="paid" value="Paid" class="ml-2">Paid
                                        <input type="checkbox" name="video" value="Video" class="ml-2">Video
                                        <input type="checkbox" name="book" value="Book" class="ml-2">Book
                                    </div>
                                </div>
                                <div class="row border-top border-bottom">
                                    <div class="col-md-4">
                                        <p class="my-2">This course is for:</p>
                                    </div>
                                    <div class="col-md-7 d-flex align-items-center">
                                        <input type="radio" name="level" value="Advanced" class="ml-2">Advanced
                                        <input type="radio" name="level" value="Beginner" class="ml-2">Beginner
                                    </div>
                                </div>
                                <div class="row border-top border-bottom">
                                    <div class="col-md-3">
                                        <p class="my-4">Course Language:</p>
                                    </div>
                                    <div class="col-md-7 d-flex align-items-center">
                                        <input type="radio" name="language" value="English" class="ml-2">English
                                        <input type="radio" name="language" value="Spanish" class="ml-2">Spanish
                                        <input type="radio" name="language" value="French" class="ml-2">French
                                        <input type="radio" name="language" value="German" class="ml-2">German
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary btn-block">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    {{-- *******Scripts --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/layout.js') }}"></script>

    @yield('scripts')
</body>

</html>
