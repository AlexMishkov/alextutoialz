@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h1>Find the Best {{ $category->name }} Courses & Tutorials</h1>
                <div class="row">
                    {{-- *******Local search bar --}}
                    <div class="col-md-6 offset-md-3 mt-4">
                        <div class="input-group mb-2 search-bar2">
                            <div class="input-group-prepend">
                                <div class="input-group-text border-right-0 bg-white"><i
                                        class="fa fa-search text-primary"></i>
                                </div>
                            </div>
                            <input class="form-control border-left-0 technology-search" type="text" placeholder="Search"
                                aria-label="Search">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer-adjustment">
            <div class="col-md-8 offset-md-2">
                <div class="row justify-content-center mt-4">
                    {{-- *******Listing all technologies under this class --}}
                    @foreach ($data as $technology)
                        <div class="col-sm-4 technology">
                            <a href="{{ route('show.tutorials', $technology->slug) }}"
                                class="text-decoration-none text-dark">
                                <div class="card m-2" style="height: 4.7rem">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <img src="{{ $technology->image }}" style="width: 30px;">
                                            </div>
                                            <div class="col-sm-10">
                                                <p>{{ $technology->name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- *******Scripts --}}
@section('scripts')
    <script src="{{ asset('js/technology.js') }}"></script>
@endsection
