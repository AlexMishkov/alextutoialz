// Local search logic
$(document).ready(function() {
    $('.technology-search').on('keyup', function() {
        let value = $(this).val().toLowerCase();
        $(".technology").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});