$(document).ready(function () {
    
    // *******Global search logic
    $.get('/global-search', function (data) {
        $.each(data, function (key, row) {
            $('#globalSearchList').append(`<li class='subClasses '><a href='/tutorials/${row.slug}' class='text-decoration-none text-dark'>${row.name}</a></li>`)
        });

        $('#globalSearch').on('keyup', function () {
            
            let value = $(this).val().toLowerCase();
            if (value != '') {
                $(".subClasses").filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
                $('#globalSearchDropdown').show();
            } else {
                $('#globalSearchDropdown').hide();
            }
        });
    });

    // *******Search logic for tutorial submit(almost the same as with global search but still different)

    $.get('/global-search', null, function (data) {
        $.each(data, function (key, row) {
            $('#globalSearchTutorialsList').append(`<li class='subClasses '><a href='' type='button' class='text-decoration-none ${row.slug} text-dark'>${row.name}</a></li>`)
            
            $("."+row.slug).on('click', function(event){
                event.preventDefault();
                $('#globalSearchTutorials').val(row.name);
            })
        });

        $('#globalSearchTutorials').on('keyup', function () {
            
            let value = $(this).val().toLowerCase();
            if (value != '') {
                $(".subClasses").filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
                $('#globalSearchTutorialsDropdown').show();
            } else {
                $('#globalSearchTutorialsDropdown').hide();
            }
        });

        
    });

    $('#login-instead').on('click', function(){
        $('#register-form').hide();
        $('#login-form').show();
    })

    $('#register-instead').on('click', function(){
        $('#login-form').hide();
        $('#register-form').show();
    })

});
