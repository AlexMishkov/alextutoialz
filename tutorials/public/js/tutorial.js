$(document).ready(function () {
    // *******Using the functions for the filters
    filterCheckbox('free');
    filterCheckbox('paid');
    filterCheckbox('video');
    filterCheckbox('book');
    levelFilterCheckbox('beginner');
    levelFilterCheckbox('advanced');
    levelFilterCheckbox('english');
    levelFilterCheckbox('spanish');
    levelFilterCheckbox('french');
    levelFilterCheckbox('german');

    // *******Adding classes to tutorials for filter to work with
    $('span.Free').parent().parent().parent().parent().parent().parent().addClass('Free');
    $('span.Video').parent().parent().parent().parent().parent().parent().addClass('Video');
    $('span.Paid').parent().parent().parent().parent().parent().parent().addClass('Paid');
    $('span.Book').parent().parent().parent().parent().parent().parent().addClass('Book');
    $('span.Beginner').parent().parent().parent().parent().parent().parent().addClass('Beginner');
    $('span.Advanced').parent().parent().parent().parent().parent().parent().addClass('Advanced');
    $('span.English').parent().parent().parent().parent().parent().parent().addClass('English');
    $('span.Spanish').parent().parent().parent().parent().parent().parent().addClass('Spanish');
    $('span.French').parent().parent().parent().parent().parent().parent().addClass('French');
    $('span.German').parent().parent().parent().parent().parent().parent().addClass('German');
    $('.tutorials').addClass('shown-tutorials');
    paginate();

    filterNumbers();

    // *******Liking a post
    $('.like-btn').on('click', function(event) {
        event.preventDefault();
        let clickedBtn = $(this)

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            }
        });

        $.ajax({
            url: "/likes",
            method: 'POST',
            data: {
                file: $(this).siblings(".tutorialId").val()
            },
        });

        $.ajax({
            url: "/return/likes",
            method: 'GET',
            data: {
                file: $(this).siblings(".tutorialId").val()
            },
            success: function(data) {
                clickedBtn.siblings('span.like-span').text(data.length)
                clickedBtn.parent().siblings().children('span.like-span').text(data.length)
                clickedBtn.parent().hide()
                clickedBtn.parent().siblings().show()

            }
        });
    });

    // *******Unsliking a post
    $('.unlike-btn').on('click', function(event) {
        event.preventDefault();
        let unlikeClickedBtn = $(this)

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            }
        });

        $.ajax({
            url: "/unlike",
            method: 'POST',
            data: {
                file: $(this).siblings(".tutorialId").val()
            },
        });

        $.ajax({
            url: "/return/likes",
            method: 'GET',
            data: {
                file: $(this).siblings(".tutorialId").val()
            },
            success: function(data) {
                unlikeClickedBtn.siblings('span.like-span').text(data.length)
                unlikeClickedBtn.parent().siblings().children('span.like-span').text(data.length)
                unlikeClickedBtn.parent().hide()
                unlikeClickedBtn.parent().siblings().show()

            }
        });
    });



    





});