// *******Pagination function
let paginate = function () {
    $('.pagination').html('')
    let trnum = 0
    let maxRows = 6
    let totalRows = $('.shown-tutorials').length

    $('.shown-tutorials').each(function () {
        trnum++
        if (trnum > maxRows) {
            $(this).hide()
        }
        if (trnum <= maxRows) {
            $(this).show()
        }
    })
    if (totalRows > maxRows) {
        let pageNum = Math.ceil(totalRows / maxRows)
        for (let i = 1; i <= pageNum;) {
            $('.pagination').append("<li class='px-2 mx-1 border rounded pagination-numbers' data-page='" + i + "'\<span>" + i++ + "<span class='sr-only'>(current)</span></span>\<li>").show()
        }
    }

    $('.pagination li:first-child').addClass('active')

    $('.pagination li').on('click', function () {
        let pageNumber = $(this).attr('data-page')
        let trIndex = 0;
        $('.pagination-numbers').removeClass('active')
        $(this).addClass('active')
        $('.shown-tutorials').hide();
        let pageTotal = maxRows * pageNumber
        for (let i = pageTotal - maxRows; i < pageTotal; i++) {
            $(".shown-tutorials:eq(" + i + ")").show();
        }
    })
}
// *******Function for the number of posts left with that class for filter to show 
let filterNumbers = function () {
    let freeNum = $('.shown-tutorials.Free').length
    let paidNum = $('.shown-tutorials.Paid').length
    let videoNum = $('.shown-tutorials.Video').length
    let bookNum = $('.shown-tutorials.Book').length
    let beginnerNum = $('.shown-tutorials.Beginner').length
    let advancedNum = $('.shown-tutorials.Advanced').length
    let englishNum = $('.shown-tutorials.English').length
    let spanishNum = $('.shown-tutorials.Spanish').length
    let frenchNum = $('.shown-tutorials.French').length
    let germanNum = $('.shown-tutorials.German').length
    $('.free-label').html("Free (" + freeNum + ")")
    $('.paid-label').html("Paid (" + paidNum + ")")
    $('.video-label').html("Video (" + videoNum + ")")
    $('.book-label').html("Book (" + bookNum + ")")
    $('.beginner-label').html("Beginner (" + beginnerNum + ")")
    $('.advanced-label').html("Advanced (" + advancedNum + ")")
    $('.english-label').html("English (" + englishNum + ")")
    $('.spanish-label').html("Spanish (" + spanishNum + ")")
    $('.french-label').html("French (" + frenchNum + ")")
    $('.german-label').html("German (" + germanNum + ")")
}

// *******Filter bar check for filtering tutorials
let checkFilter = function () {
    if ($('.filter-bar-content').children().length != 0) {
        $('.filter-bar').removeClass("d-none");
        $('.filter-bar').addClass("d-flex");
    } else {
        $('.filter-bar').removeClass("d-flex");
        $('.filter-bar').addClass("d-none");
    }

}

// *******Function for filtering the tutorials
let filterLogic = function () {

    let spanNum = $("#filter-bar-content span").length;
    if (spanNum < 1) {
        $(".tutorials").show();
        $(".tutorials").addClass('shown-tutorials');
    } else {
        let filterBarArr = [];
        $('.tutorials').removeClass('shown-tutorials');
        $('.tutorials').hide();

        for (let i = 0; i < spanNum; i++) {
            filterBarArr.push($("#filter-bar-content span:eq(" + i + ")").html())
        }
        let myClassStr = '';
        $.each(filterBarArr, function (key, myClass) {
            myClassStr += "." + myClass;
        });
        $("div" + myClassStr).show();
        $("div" + myClassStr).addClass('shown-tutorials');
    }
    paginate()
    filterNumbers();
}

// *******Capitalize function
let strCapitalize = function (str) {
    let strCapitalized = str.charAt(0).toUpperCase() + str.slice(1);
    return strCapitalized;
}

// Function for adding filter tags in filter bar
let addSpan = function (addClass) {
    let myStr = strCapitalize(addClass);
    $('.filter-bar-content').append($("<span class='border ml-3 text-capitalize filter-span rounded text-primary " + addClass + "-filter'>" + myStr + "</span>"));
    $("." + addClass + "-filter").on('click', function () {
        $(this).remove();
        $("#" + addClass).prop('checked', false);
        if (addClass == 'beginner') {
            $('#advanced').parent().removeClass('d-none');
            $('#advanced').parent().addClass('d-flex');
        } else if (addClass == 'advanced') {
            $('#beginner').parent().removeClass('d-none');
            $('#beginner').parent().addClass('d-flex');
        } else if (addClass == 'english') {
            $('#spanish').parent().removeClass('d-none');
            $('#spanish').parent().addClass('d-flex');
            $('#french').parent().removeClass('d-none');
            $('#french').parent().addClass('d-flex');
            $('#german').parent().removeClass('d-none');
            $('#german').parent().addClass('d-flex');
        } else if (addClass == 'spanish') {
            $('#english').parent().removeClass('d-none');
            $('#english').parent().addClass('d-flex');
            $('#french').parent().removeClass('d-none');
            $('#french').parent().addClass('d-flex');
            $('#german').parent().removeClass('d-none');
            $('#german').parent().addClass('d-flex');
        } else if (addClass == 'french') {
            $('#spanish').parent().removeClass('d-none');
            $('#spanish').parent().addClass('d-flex');
            $('#english').parent().removeClass('d-none');
            $('#english').parent().addClass('d-flex');
            $('#german').parent().removeClass('d-none');
            $('#german').parent().addClass('d-flex');
        } else if (addClass == 'german') {
            $('#spanish').parent().removeClass('d-none');
            $('#spanish').parent().addClass('d-flex');
            $('#french').parent().removeClass('d-none');
            $('#french').parent().addClass('d-flex');
            $('#english').parent().removeClass('d-none');
            $('#english').parent().addClass('d-flex');
        }
        checkFilter();
        filterLogic();
    });
}

// *******Removing filter tags from filter bar
let removeSpan = function (removeClass) {
    $('.filter-bar-content').find("." + removeClass + "-filter").remove();
}

// When removed tags from filter bar by clicking unchecking the checkbox on the filter
let filterCheckbox = function (myClass) {
    $('#' + myClass).on('change', function (e) {
        if ($(this).prop('checked')) {
            addSpan(myClass);
        } else {
            removeSpan(myClass);
        }

        checkFilter();
        filterLogic();
    });
}

// *******Logic for the filter to remove checkboxes that cant be selected together
let levelFilterCheckbox = function (myClass) {
    $('#' + myClass).on('change', function (e) {
        if ($(this).prop('checked')) {
            addSpan(myClass);
            if (myClass == 'beginner') {
                $('#advanced').parent().removeClass('d-flex');
                $('#advanced').parent().hide();
            } else if (myClass == 'advanced') {
                $('#beginner').parent().removeClass('d-flex');
                $('#beginner').parent().hide();
            } else if (myClass == 'english') {
                $('#spanish').parent().removeClass('d-flex');
                $('#spanish').parent().hide();
                $('#french').parent().removeClass('d-flex');
                $('#french').parent().hide();
                $('#german').parent().removeClass('d-flex');
                $('#german').parent().hide();
            } else if (myClass == 'spanish') {
                $('#english').parent().removeClass('d-flex');
                $('#english').parent().hide();
                $('#french').parent().removeClass('d-flex');
                $('#french').parent().hide();
                $('#german').parent().removeClass('d-flex');
                $('#german').parent().hide();
            } else if (myClass == 'french') {
                $('#spanish').parent().removeClass('d-flex');
                $('#spanish').parent().hide();
                $('#english').parent().removeClass('d-flex');
                $('#english').parent().hide();
                $('#german').parent().removeClass('d-flex');
                $('#german').parent().hide();
            } else if (myClass == 'german') {
                $('#spanish').parent().removeClass('d-flex');
                $('#spanish').parent().hide();
                $('#french').parent().removeClass('d-flex');
                $('#french').parent().hide();
                $('#english').parent().removeClass('d-flex');
                $('#english').parent().hide();
            }
        } else {
            removeSpan(myClass);
            if (myClass == 'beginner') {
                $('#advanced').parent().removeClass('d-none');
                $('#advanced').parent().addClass('d-flex');
            } else if (myClass == 'advanced') {
                $('#beginner').parent().removeClass('d-none');
                $('#beginner').parent().addClass('d-flex');
            } else if (myClass == 'english') {
                $('#spanish').parent().removeClass('d-none');
                $('#spanish').parent().addClass('d-flex');
                $('#french').parent().removeClass('d-none');
                $('#french').parent().addClass('d-flex');
                $('#german').parent().removeClass('d-none');
                $('#german').parent().addClass('d-flex');
            } else if (myClass == 'spanish') {
                $('#english').parent().removeClass('d-none');
                $('#english').parent().addClass('d-flex');
                $('#french').parent().removeClass('d-none');
                $('#french').parent().addClass('d-flex');
                $('#german').parent().removeClass('d-none');
                $('#german').parent().addClass('d-flex');
            } else if (myClass == 'french') {
                $('#spanish').parent().removeClass('d-none');
                $('#spanish').parent().addClass('d-flex');
                $('#english').parent().removeClass('d-none');
                $('#english').parent().addClass('d-flex');
                $('#german').parent().removeClass('d-none');
                $('#german').parent().addClass('d-flex');
            } else if (myClass == 'german') {
                $('#spanish').parent().removeClass('d-none');
                $('#spanish').parent().addClass('d-flex');
                $('#french').parent().removeClass('d-none');
                $('#french').parent().addClass('d-flex');
                $('#english').parent().removeClass('d-none');
                $('#english').parent().addClass('d-flex');
            }
        }
        filterLogic();
        checkFilter();

    });
}
