<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return redirect(route('programming'));
})->name('home');

Route::group(['middleware' => ['auth','checkAdmin']], function () {
    Route::get('/admin/unapproved', 'AdminController@index')->name('unapproved.tutorials');
    Route::put('/admin/approve/{tutorial}', 'AdminController@approveTutorial')->name('approve.tutorial');
    Route::delete('/admin/reject', 'AdminController@rejectTutorial')->name('reject.tutorial');
    Route::delete('/admin/delete/{tutorial}', 'AdminController@deleteTutorial')->name('delete.tutorial');
});
Route::post('/tutorial/store', 'TutorialController@storeTutorial')->name('store.tutorial')->middleware('auth');

Route::post('/approve-mail', 'MailController@approveMail')->name('approve-mail');



Route::get('/programming', 'HomeController@showProgramming')->name('programming');
Route::get('/data-science', 'HomeController@showDataScience')->name('data-science');
Route::get('/devops', 'HomeController@showDevops')->name('devops');
Route::get('/design', 'HomeController@showDesign')->name('design');
Route::get('/tutorials/{slug}', 'TutorialController@showTutorials')->name('show.tutorials');
Route::get('/global-search', 'SearchController@globalSearch')->name('global.search');

// Likes
Route::post('/likes', 'LikesController@likes')->name('likes');
Route::get('/return/likes', 'LikesController@getLikes')->name('return.likes');
Route::post('/unlike', 'LikesController@unlike')->name('unlike');


// Github Socilite
Route::get('login/github', 'Auth\LoginController@githubProvider')->name('github.login');
Route::get('login/github/callback', 'Auth\LoginController@githubCallback');

// Facebook Socilite
Route::get('login/facebook', 'Auth\LoginController@facebookProvider')->name('facebook.login');
Route::get('login/facebook/callback', 'Auth\LoginController@facebookCallback');

// Google Socilite
Route::get('login/google', 'Auth\LoginController@googleProvider')->name('google.login');
Route::get('login/google/callback', 'Auth\LoginController@googleCallback');

